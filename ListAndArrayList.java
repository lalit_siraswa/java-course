import java.awt.*;
import java.util.ArrayList;
import java.util.Scanner;

public class ListAndArrayList {
    //========================================================
//    private static Scanner sc = new Scanner(System.in);
//    private static int[] baseData = new int[10];
//    public static void main(String args[]){
//        System.out.println("Enter 10 integers : ");
//        getInput();
//        printArray();
//        resizeArray();
//        printArray();
//        System.out.println("\nEnter 12 integers : ");
//        getInput();
//        printArray();
//    }
//    private static void getInput(){
//        for(int i = 0; i < baseData.length; i++)
//            baseData[i] = sc.nextInt();
//    }
//    public static void printArray(){
//        for(int i = 0; i < baseData.length; i++)
//            System.out.print(baseData[i] + "  ");
//    }
//    public static void resizeArray(){
//        int[] original = baseData;
//        baseData = new int[12];
//        for(int i = 0; i < original.length; i++)
//            baseData[i] = original[i];
//    }
    //========================================================
//    public static void main(String args[]){
//        ArrayList<Integer> myList = new ArrayList<Integer>();
//        myList.add(10);
//        myList.add(20);
//        myList.add(30);
//        myList.add(40);
//        myList.add(50);
////        for(int item : myList)
////            System.out.println(item);
//        System.out.println(myList.size());
//    }
    //=========================================================
//    private static ArrayList<String> groceryList = new ArrayList<String>();
//    public static void main(String args[]){
//        addGroceryItem("Banana");
//        addGroceryItem("Orange");
//        addGroceryItem("Apple");
//        addGroceryItem("Sugar");
//        addGroceryItem("Salt");
//        groceryList.add(1, "Rise");
//        groceryList.set(2, "Grapes");
//        printGroceryItem();
//    }
//    public static void addGroceryItem(String item){
//        groceryList.add(item);
//    }
//    public static void printGroceryItem(){
//        for(int i = 0; i < groceryList.size(); i++){
//            System.out.println((i+1) + " : " + groceryList.get(i));
//        }
//    }
    //===================================================================
//    private static ArrayList<Integer> myList = new ArrayList<Integer>();
//    public static void main(String args[]){
//        myList.add(2);
//        myList.add(4);
//        myList.add(6);
//        myList.add(8);
////        System.out.println(myList.contains(5));
////        System.out.println(myList.contains(2));
////        System.out.println(myList.contains(10));
//        System.out.println(myList.indexOf(10));
//        System.out.println(myList.indexOf(6));
//        System.out.println(myList.indexOf(12));
        //============================================
//        myList.add(0, 10);
//        myList.add(1, 20);
//        myList.add(2, 30);
//        myList.add(3, 40);
//        printList();
//        myList.set(0, 2);
//        myList.set(1, 4);
//        myList.set(2, 6);
//        myList.set(3, 8);
//        printList();
//        System.out.println(myList.remove(0));
//        printList();
//    }
//    public static void printList(){
//        for(int i = 0; i < myList.size(); i++){
//            System.out.println(i + " : " + myList.get(i));
//        }
//    }
    public static void main(String args[]){
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(2);
        list.add(4);
        list.add(6);
        list.add(8);
        System.out.println(list.toString());
        list.set(0, 10);
        list.set(1, 20);
        list.set(2, 30);
        list.set(3, 40);
        System.out.println(list.toString());
        System.out.println(list.contains(40));
        System.out.println(list.contains(50));
        System.out.println(list.indexOf(30));
        System.out.println(list.indexOf(50));
        list.remove(2);
        System.out.println(list.toString());
        list.add(2, 30);
        System.out.println(list.toString());
        System.out.println(list.isEmpty());
        System.out.println(list.size());
    }
}
