import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

public class LinkedListPractice {
    public static void main(String[] args) {
        LinkedList<String> placesToVisit = new LinkedList<String>();
        addInOrder(placesToVisit, "xyz");
        addInOrder(placesToVisit, "abc");
        addInOrder(placesToVisit, "ijk");
        addInOrder(placesToVisit, "efg");
        addInOrder(placesToVisit, "abc");

        printList(placesToVisit);

        //-------------------------------------
//        placesToVisit.add("A");
//        placesToVisit.add("B");
//        placesToVisit.add("C");

        //--------------------------------------
//        Iterator<String> itr = placesToVisit.iterator();
//        System.out.println(itr.next());
//        System.out.println(itr.next());
//        System.out.println(itr.); // has no .previous() method that's why we need to use ListIterator
        //---------------------------------------
//        ListIterator<String> itr = placesToVisit.listIterator();
//        System.out.println(itr.next());
//        System.out.println(itr.next());
//        System.out.println(itr.previous());
//        System.out.println(itr.previous());
        //-----------------------------------------
    }
    private static void printList(LinkedList<String> linkedList){
        Iterator<String> iterator = linkedList.iterator();
        while(iterator.hasNext()){
            System.out.println("Now visiting : " + iterator.next());
        }
    }
    private static boolean addInOrder(LinkedList<String> linkedList, String newCity){
        ListIterator<String> stringListIterator = linkedList.listIterator();
        while(stringListIterator.hasNext()){
            int comparison = stringListIterator.next().compareTo(newCity);
            if(comparison == 0){
                // equal do not add
                System.out.println(newCity + " is already included as a destination.");
                return false;
            }
            else if(comparison > 0){
                // newCity should appear before this one
                stringListIterator.previous();
                stringListIterator.add(newCity);
                return true;
            }
        }
        stringListIterator.add(newCity);
        return true;
    }

    //================================================================
//    public static void main(String[] args) {
//        LinkedList<String> placesToVisit = new LinkedList<String>();
//        placesToVisit.add("Jhunjhunu");
//        placesToVisit.add("Nawalgarh");
//        placesToVisit.add("Sikar");
//        placesToVisit.add("Neem Ka Thana");
//        placesToVisit.add("Chomu Puliya");
//        placesToVisit.add("Jaipur");
//        placesToVisit.add("Delhi");

//        printList(placesToVisit);
//        placesToVisit.add(3, "Khatushayam ji");
//        printList(placesToVisit);
//        placesToVisit.add(7, "ISCON Temple");
//        printList(placesToVisit);
//
//        placesToVisit.remove(1);
//        printList(placesToVisit);
//        placesToVisit.remove(3);
//        printList(placesToVisit);

//        System.out.println(placesToVisit.size());
//        System.out.println(placesToVisit.getLast());
//    }
//    private static void printList(LinkedList<String> linkedList){
//        Iterator<String> i = linkedList.iterator();
//        System.out.println("==========================================");
//        while(i.hasNext()){
//            System.out.println("Now visiting : " + i.next());
//        }
//        System.out.println("==========================================");
//    }
}
