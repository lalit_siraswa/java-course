import java.util.ArrayList;

public class MobilePhone {
    private String myNumber;
    ArrayList<Contact> myContacts;
    public MobilePhone(String myNumber){
        this.myNumber = myNumber;
        myContacts  = new ArrayList<Contact>();
    }
    public boolean addNewContact(Contact contact){
        for(int i = 0; i < myContacts.size(); i++){
            if(myContacts.get(i).getName().equals(contact.getName()))
                return false;
        }
        myContacts.add(contact);
        return true;
    }
    public boolean updateContact(Contact old, Contact newOne){
        int index = myContacts.indexOf(old);
        if(index >= 0){
            myContacts.set(index, newOne);
            return true;
        }
        else
            return false;
    }
    public boolean removeContact(Contact contact){
        int index = myContacts.indexOf(contact);
        if(index >= 0){
            myContacts.remove(index);
            return true;
        }
        else
            return false;
    }
    private int findContact(Contact contact){
        return myContacts.indexOf(contact);
    }
    private int findContact(String name){
        Contact contact;
        for(int i = 0; i < myContacts.size(); i++){
            contact = myContacts.get(i);
            if(contact.getName().equals(name)){
                return i;
            }
        }
        return -1;
    }
    public Contact queryContact(String name){
        Contact contact;
        for(int i = 0; i < myContacts.size(); i++){
            contact = myContacts.get(i);
            if(contact.getName().equals(name)){
                return contact;
            }
        }
        return null;
    }
    public void printContacts(){
        Contact contact;
        for(int i = 0; i < myContacts.size(); i++){
            contact = myContacts.get(i);
            System.out.println(i+1 + ". " + contact.getName() + "->" + contact.getPhoneNumber());
        }
    }
}
