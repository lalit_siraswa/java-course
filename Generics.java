import java.util.ArrayList;

public class Generics {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);

        printDoubled(list);
    }
    private static void printDoubled(ArrayList list){
        for(Object i : list){
            System.out.println((Integer)i*2);
        }
    }
}
