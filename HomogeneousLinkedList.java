import java.util.ArrayList;

public class HomogeneousLinkedList {
    //=======================================================
//    public static void main(String[] args) {
//        ArrayList list = new ArrayList();
//        list.add("Lalit Siraswa");
//        list.add(101);
//        list.add(345.789);
//        list.add('L');
//
//        printList(list);
//    }
//    private static void printList(ArrayList list){
//        for(Object i : list){
//            System.out.println(i);
//        }
//    }
    //=======================================================
    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        printDoubled(list);
    }
    private static void printDoubled(ArrayList<Integer> list){
        for(int i : list)
            System.out.println(i*2);
    }
}
