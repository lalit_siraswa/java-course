import java.util.LinkedList;
import java.util.ListIterator;

public class ListIteratorPractice {
    public static void main(String[] args) {
        LinkedList<String> placesToVisit = new LinkedList<String>();
        placesToVisit.add("ABC");
        placesToVisit.add("BCD");
        placesToVisit.add("CDE");
        placesToVisit.add("DEF");
        ListIterator<String> itr = placesToVisit.listIterator();
        System.out.println(itr.next());
        System.out.println(itr.next());
        System.out.println(itr.previous());
//        printList(placesToVisit);
    }
    private static void printList(LinkedList<String> list){
        ListIterator<String> itr = list.listIterator();
        while (itr.hasNext()){
            System.out.println("now visiting : " + itr.next());
        }
    }
}
