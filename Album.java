import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
    public static void main(String[] args) {
        ArrayList<Album> albums = new ArrayList<Album>();
        Album album = new Album("Stormbringer", "Deep Purple");
        album.addSong("ABC", 4.6);
        album.addSong("BCD", 5.7);
        album.addSong("CDE", 4.2);
        album.addSong("EFG", 5.6);
        album.addSong("FGH", 3.4);
        album.addSong("GHI", 5.7);
        album.addSong("IJK", 3.2);
        album.addSong("JKL", 4.3);
        album.addSong("KLM", 2.5);
        albums.add(album);

        album = new Album("For those about rock", "AC/DC");
        album.addSong("KLM", 4.6);
        album.addSong("LMN", 4.6);
        album.addSong("MNO", 4.6);
        album.addSong("NOP", 4.6);
        album.addSong("OPQ", 4.6);
        album.addSong("PQR", 4.6);
        albums.add(album);

        LinkedList<Song> playList = new LinkedList<Song>();
        albums.get(0).addToPlayList("IJK", playList);
        albums.get(0).addToPlayList("KLM", playList);
        System.out.println(albums.get(0).addToPlayList("ZAB", playList));

        System.out.println(albums.get(0).addToPlayList(9, playList));
        System.out.println(albums.get(1).addToPlayList(1, playList));
        System.out.println(albums.get(1).addToPlayList(34, playList));
    }
    private String name, artist;
    private ArrayList<Song> songs;
    public Album(String name, String artist){
        this.name = name;
        this.artist = artist;
        songs = new ArrayList<Song>();
    }
    public boolean addSong(String title, double duration){
        for(int i = 0; i < songs.size(); i++){
            if(songs.get(i).getTitle().equals(title))
                return false;
        }
        songs.add(new Song(title, duration));
        return true;
    }
    private Song findSong(String title){
        for(int i = 0; i < songs.size(); i++){
            if(songs.get(i).getTitle().equals(title))
                return songs.get(i);
        }
        return null;
    }
    public boolean addToPlayList(int trackNumber, LinkedList<Song> playList){
        if(trackNumber == 0)
            return false;
        if(songs.size()+1 < trackNumber)
            return false;
        playList.add(songs.get(trackNumber-1));
        return true;
    }
    public boolean addToPlayList(String title, LinkedList<Song> playList){
        Song song = findSong(title);
        if(song != null){
            playList.add(song);
            return true;
        }
        return false;
    }
}
