import java.util.Scanner;

public class MinimumElement {
    public static void main(String args[]){

    }
    private static int readInteger(){
        Scanner sc = new Scanner(System.in);
        int num = sc.nextInt();
        return num;
    }
    private static int[] readElements(int num){
        Scanner sc = new Scanner(System.in);
        int[] array = new int[num];
        for(int i = 0; i < num; i++)
            array[i] = sc.nextInt();
        return array;
    }
    private int findMin(int[] array){
        int minimum = array[0];
        for(int i = 1; i < array.length; i++)
            if(minimum >= array[i])
                minimum = array[i];
        return minimum;
    }
}
