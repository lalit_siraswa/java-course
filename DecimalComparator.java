public class DecimalComparator {
    public static void main(String[] args) {
//        System.out.println(areEqualByThreeDecimalPlaces(-3.175, -3.1356));
        System.out.println(areEqualByThreeDecimalPlaces(3.175, 3.176));

//----------------------------------------------------
//        int x = (int)(-3.175*10);
//        System.out.println((int)(-3.175*10));
//----------------------------------------------------
    }
    private static boolean areEqualByThreeDecimalPlaces(double first, double second){
        if(first == second)
            return true;
        int num1, num2;
        for(int i = 1; i <= 3; i++){
            num1 = (int)(first*10);
            num2 = (int)(second*10);
//            System.out.println(num1 + " : " + num2);
            first = first * 10;
            second = second * 10;
            if(num1 != num2)
                return false;
        }
        return true;
    }
}
