import java.util.ArrayList;

public class Branch {
    private String name;
    private ArrayList<Customer> customers;
    public Branch(String name){
        this.name = name;
        customers = new ArrayList<Customer>();
    }
    public String getName(){
        return this.name;
    }
    public ArrayList<Customer> getCustomers(){
        return customers;
    }
    public boolean newCustomer(String name, double transaction){
        for(int i = 0; i < customers.size(); i++){
            if(customers.get(i).getName().equals(name))
                return false;
        }
        Customer customer = new Customer(name, transaction);
        customers.add(customer);
        return true;
    }
    public boolean addCustomerTransaction(String name, double transaction){
        for(int i = 0; i < customers.size(); i++){
            if(customers.get(i).getName().equals(name)){
                customers.get(i).addTransaction(transaction);
                return true;
            }
        }
        return false;
    }
    private Customer findCustomer(String name){
        Customer customer;
        for(int i = 0; i < customers.size(); i++){
            customer = customers.get(i);
            if(customer.getName().equals(name)){
                return customer;
            }
        }
        return null;
    }
}
