import java.util.ArrayList;

public class Bank{
    private String name;
    private ArrayList<Branch> branches;
    public Bank(String name){
        this.name = name;
        branches = new ArrayList<Branch>();
    }
    public boolean addBranch(String name){
        for(int i = 0; i < branches.size(); i++){
            if(branches.get(i).getName().equals(name))
                return false;
        }
        Branch branch = new Branch(name);
        return branches.add(branch);
    }
    public boolean addCustomer(String branchName, String cusName, double transaction){
        ArrayList<String> branchNames = new ArrayList<String>();
        for (int i = 0; i < branches.size(); i++){
            branchNames.add(branches.get(i).getName());
        }
        if(!(branchNames.contains(branchName)))
            return false;
        for(int i = 0; i < branches.size(); i++){
            if(branches.get(i).getName().equals(branchName)){
                ArrayList<Customer> customer;
                customer = branches.get(i).getCustomers();
                for(int j = 0; j < customer.size(); j++){
                    if(customer.get(j).getName().equals(cusName))
                        return false;
                }
                branches.get(i).newCustomer(cusName, transaction);
                break;
            }
        }
        return true;
    }
    public boolean addCustomerTransaction(String branchName, String cusName, double transaction){
        for(int i = 0; i < branches.size(); i++){
            if(branches.get(i).getName().equals(branchName)){
                for(int j = 0; j < branches.get(i).getCustomers().size(); j++){
                    if(branches.get(i).getCustomers().get(j).getName().equals(cusName)){
                        branches.get(i).getCustomers().get(j).addTransaction(transaction);
                        return true;
                    }
                }
            }
        }
        return false;
    }
    private Branch findBranch(String name){
        for(int i = 0; i < branches.size(); i++){
            if(branches.get(i).getName().equals(name))
                return branches.get(i);
            return null;
        }
        return null;
    }
    public boolean listCustomers(String branchName, boolean transaction){
        for(int i = 0; i < branches.size(); i++){
            if(branches.get(i).getName().equals(branchName)){
                System.out.println("Customer details for branch " + branchName);
                ArrayList<Customer> customers = branches.get(i).getCustomers();
                for(int j = 0; j < customers.size(); j++){
                    System.out.println("Customer: "+customers.get(j).getName()+"["+j+1+"]");
                    if(transaction){
                        System.out.println("Transaction");
                        ArrayList<Double> trans;
                        trans = customers.get(j).getTransactions();
                        for(int k = 0; k < trans.size(); k++){
                            System.out.println("["+k+1+"] Amount "+trans.get(k));
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }
}
