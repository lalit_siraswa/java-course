import java.util.LinkedList;
import java.util.ListIterator;

public class IteratorPractice {
    public static void main(String[] args) {
        LinkedList<String> list = new LinkedList<String>();
        ListIterator<String> itr = list.listIterator();
        itr.add("ABC");
        itr.add("BCD");
        itr.add("CDE");
        itr.add("DEF");
//        ListIterator<String> itr = list.listIterator();

//        while(itr.hasNext()){
//            System.out.println(itr.next());
//        }
//        System.out.println(itr.next());
//        System.out.println(itr.next());
        printList(list);
    }
    private static void printList(LinkedList<String> list){
        ListIterator<String> itr = list.listIterator();
        while(itr.hasNext()){
            System.out.println(itr.next());
        }
    }
}
