import java.util.Scanner;

public class SortedArray {
    public static void main(String args[]){
        int size;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the size of the array : ");
        size = sc.nextInt();
        System.out.println("Enter array elements : ");
        int arr[] = getIntegers(size);
        printArray(arr);
        arr = sortIntegers(arr);
        printArray(arr);
    }
    public static int[] getIntegers(int size){
        Scanner sc = new Scanner(System.in);
        int element;
        int arr[] = new int[size];
        for(int i = 0; i < size; i++){
            element = sc.nextInt();
            arr[i] = element;
        }
        return arr;
    }
    public static void printArray(int []arr){
        for(int i = 0; i < arr.length; i++)
            System.out.println("Element " + i + " contents " + arr[i]);
    }
    public static int[] sortIntegers(int []arr){
        for(int i = 0; i < arr.length; i++){
            for(int j = 0; j < arr.length; j++){
                if(arr[i] > arr[j]){
                    int temp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = temp;
                }
            }
        }
        return arr;
    }
}
